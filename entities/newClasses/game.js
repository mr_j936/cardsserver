
var game = function() {
    this.tables = [];

    this.addTable = function(table) {
        this.tables.push(table);
    
    };
    this.removeTable = function(table) {
        for (var i = 0; i < this.tables.length; i++) {
            if (this.tables[i] == table)
            {
                this.tables.splice(i, 1);
              
            }
        }

    };
    this.getTables = function() {
        var data = [];
        for (var i = 0; i < this.tables.length; i++) {
            console.log(this.tables[i].hasVacant());
            if ( this.tables[i].visibility == "public"&&this.tables[i].hasVacant())
                data.push({position: i, tableName: this.tables[i].tableName, players: this.tables[i].getSeatsInfo()});
        }
        return data;
    };

    this.findTable = function(tableName) {
        for (var i = 0; i < this.tables.length; i++) {
            if (this.tables[i].tableName == tableName) {
                return this.tables[i];
            }
        }
        return false;
    }
}








module.exports = game;



