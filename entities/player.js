
var player = function() {
    this.cards = [];
    this.name = "anonymous";
    this.socket = null;
    this.table = null;
    this.turn = false;
    this.points = 0;
    this.position = null;
    this.bid = null;
    this.score;
    this.id = null;
    this.food = [];
//setters and getters
    this.setCards = function(cards) {
        this.cards = cards;
    }
    this.getCards = function() {
        return this.cards;
    }
    this.setName = function(name) {
        this.name = name;
    }
    this.getName = function() {
        return this.name;
    }
    this.setSocket = function(socket) {
        this.socket = socket;
    }
    this.getSocket = function() {
        return this.socket;
    }
    this.setTable = function(table) {
        this.table = table;
    }
    this.getTable = function() {
        return this.table;
    }
    this.setTurn = function(turn) {
        this.turn = turn;
    }
    this.getTurn = function() {
        return this.turn;
    }
    this.setPoints = function(points) {
        this.points = points;
    }
    this.getPoints = function() {
        return this.points;
    }
    this.setPosition = function(position) {
        this.position = position;
    }
    this.getPosition = function() {
        return this.position;
    }
    this.setBid = function(bid) {
        this.bid = bid;
    }
    this.getBid = function() {
        return this.bid;
    }
    this.setScore = function(score) {
        this.score = score;
    }
    this.getScore = function() {
        return this.score;
    }
    this.setId = function(id) {
        this.id = id;
    }
    this.getId = function() {
        return this.id;
    }
    this.setFood = function(food) {
        this.food = food;
    }
    this.getFood = function() {
        return this.food;
    }
    this.send = function(key, value) {
        if (this.socket != null)
        {
            this.socket.emit(key, value);
        }
    }

    this.resetPlayer = function() {
        this.food = [];
        this.turn = false;
        this.points = 0;
        this.bid = null;
        this.position = null;
         this.cards = [];
    }
    this.findCard = function(card) {
        var cards = this.getCards();
        for (var i = 0; i < cards.length; i++) {
            if (cards[i] == card) {
                return true;
            }
        }
        return false;
    };
    this.playCard = function(card) {
        var cards = this.getCards();

        if (this.turn && this.isLegal(card)) {
            for (var i = 0; i < cards.length; i++) {
                if (cards[i] == card) {
                    this.cards.splice(i, 1);
                    this.getTable().addCard(card, this);
                    if (this.getTable().cards.length == 4)//this is the fourth card played, lets decide who takes the hand
                    {
                        var winner = this.decideWinner();
                        console.log("winner is " + winner.position);
                    
                       
                        for (var i = 0; i < this.getTable().cards.length; i++) {
                            winner.getFood().push(this.getTable().cards[i][0]);
                        }
                        this.getTable().cards = [];
    this.getTable().giveTurn(winner);
               
                    }

                    else {
                        this.getTable().nextTurn();
                    }
                    console.log("legal play");
                    this.getTable().broadcast("cardPlay", {player: this.position, card: card});
                    return true;
                }
            }
        }
        return false;
    };
    this.decideWinner = function() {
        var cards = this.getTable().getCards();
        var originalColor = getColor(cards[0][0]);

        if (originalColor != "spades") {//if it is equal to spades anyway avoid an unecessary loop. this if statement can be removed but is there for optimization
            for (var i = 0; i < cards.length; i++)
            {
                if (getColor(cards[i][0]) == "spades") {//there is a spade here
                    originalColor = "spades";
                }
            }
        }
        //if there is not a spade original color remains the same, if there was at least one spade played the original color turns to spades
        var max = 0;

        for (var i = 0; i < cards.length; i++)
        {//console.log(getValue(cards[i][0]));

            if (getColor(cards[i][0]) === originalColor && max == 0) {
                max = cards[i];//if max is not set, set this value to be the max
                continue;
            }
            //    console.log(getValue(max[0][0]));
            if (getValue(cards[i][0]) > getValue(max[0]) && getColor(cards[i][0]) == originalColor) {//if you find a bigger card replace the max
                max = cards[i];

            }

        }
        return max[1];
    }
    this.isLegal = function(card) {

        if (this.getTable().cards.length == 0) {
            return true;
        }//first player can play whatever

        else {
            var originalColor = getColor(this.getTable().cards[0][0]);//ideally it should match the color of the first card played
            var color = getColor(card);
            if (color == originalColor) {
                return true;
            }
            else {
                for (var i = 0; i < this.cards.length; i++)//scan the player's cards looking for a card that matches original color
                {
                    if (getColor(this.cards[i]) == originalColor)   //he has a color of the original color so he must play that
                    {
                        return false;
                    }
                }
                return true;//if not then play whatever... I don't care
            }
        }
        return false;//he must have returned something at this point but if not, return false
    };

    function getValue(num) {
        while ((num - 13) > -1) {
            num = num - 13;
        }
        if ((num + 1) % 13 == 0) {
            this.value = 2;
            return this.value;
        }
        if ((num + 1) % 12 == 0) {
            this.value = 3;
            return this.value;
        }
        if ((num + 1) % 11 == 0) {
            this.value = 4;
            return this.value;
        }
        if ((num + 1) % 10 == 0) {
            this.value = 5;
            return this.value;
        }
        if ((num + 1) % 9 == 0) {
            this.value = 6;
            return this.value;
        }
        if ((num + 1) % 8 == 0) {
            this.value = 7;
            return this.value;
        }
        if ((num + 1) % 7 == 0) {
            this.value = 8;
            return this.value;
        }
        if ((num + 1) % 6 == 0) {
            this.value = 9;
            return this.value;
        }
        if ((num + 1) % 5 == 0) {
            this.value = 10;
            return this.value;
        }
        if ((num + 1) % 4 == 0) {
            this.value = 11;
            return this.value;
        }
        if ((num + 1) % 3 == 0) {
            this.value = 12;
            return this.value;
        }
        if ((num + 1) % 2 == 0) {
            this.value = 13;
            return this.value;
        }
        if (num == 0) {

            this.value = 14;

            return this.value;

        }
    }
    ;

    function getColor(num) {
        var i = 0;
        var color = "";
        while (num - 12 > 0 && i < 4) {
            num = num - 13;
            i++;
        }

        if (i == 0) {
            color = "clubs";
        }
        if (i == 1) {
            color = "spades";
        }
        if (i == 2) {
            color = "diamonds";
        }
        if (i == 3) {
            color = "hearts";
        }
        this.color = color;
        return color;
    }
    ;
}



module.exports = player;











