var humanPlayer = function(socket, name, redis) {//this class is the link between human/AI users and the table. The table does not care if this user is an AI or human 
    this.name = name;
    this.seat; //the seat, if he is on a table he will have a seat to control the table. Player has one seat only. 
    this.socket = socket;
    this.id = makename();//this is a randomly generated id for the player so that we can give him his own unique redis channel to feed him information;
    this.pubClient = redis.createClient();
    this.subClient = redis.createClient();
    this.seatId = null;
    this.subClient.subscribe(this.id);
    this.unsubscribe = function() {
        this.subClient.unsubscribe('message');
    };
    var me = this;
    this.subClient.on('message', function(channel, message) {

        var data = JSON.parse(message);
        if (data.key == 'seat') {
            me.seatId = data.value.seatId;
            //console.log("received seat id " + me.seatId);
        } else {
                //  console.log("emiting key " + data.key + " and value " + data.value);
            me.socket.emit(data.key, data.value);
      
        }
    });





    this.socket.on('playCard', function(data) {
        //console.log("event triggered");
        if (me.seatId != null)//check if he is sitting anywhere
        {
          //  console.log("publishing card play");
            //console.log(me.seatId);
            var message = JSON.stringify({key: 'playCard', value: data})
            me.pubClient.publish(me.seatId, message);
            // me.seat.playCard(data);
        }
    });

    this.socket.on('bid', function(data) {
//        console.log("received the dumbass bid");
//        console.log("seat id " + this.seatId);
        //check if he is allowed to play the card
        //  console.log("seat is " + me.seat);
        if (me.seatId != null)//check if he is sitting anywhere
        {
//            console.log("forwarding the bid data to the seat");
            var message = JSON.stringify({key: 'bid', value: data});
            me.pubClient.publish(me.seatId, message);
        }
//        if (me.seat != null && me.seat.turn && me.seat.table.bidding) {
//            me.seat.bid = data;
//           // me.send('playerJoined', {tableName: me.seat.table.tableName, players: me.seat.table.getSeatsInfo()}); you can't tell yourself what you do not know
//            me.seat.table.countBid();
//        }
    });

//        fix this please

    playCard = function(card) {
        this.seat.playCard(card);
    }
    setTurn = function(turn, bidding) {
        this.send("turn", {turn: turn, bidding: bidding});
    }

    this.joinTable = function() {
        this.socket.emit('joinTable', {tableName: this.seat.table.tableName, position: this.seat.position, players: this.seat.getSeatsInfo()});
    }

    this.send = function(key, value) {
        this.socket.emit(key, value);
    }
    this.leave = function() {
        //console.log("player is leaving ");
      //  console.log(this.seatId);
        if (this.seatId != null) {
            //this.seat.vacateSeat();
            var message = JSON.stringify({key: 'vacateSeat', value: {playerId: me.id}});
            me.pubClient.publish(me.seatId, message);
            this.seatId = null;
        }
    }

    function makename() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 10; i++)//increased from 7 to 10 to make it even less likely that 2 players will have same id, and so it would not cross with table and seat names
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }
    ;

}

module.exports = humanPlayer;