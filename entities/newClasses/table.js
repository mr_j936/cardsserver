var seat = require('./seat');

var table = function() {
    this.seats = [];
    this.cards = [];
    this.maxScore = null;
    this.visibility = null;
    this.tableName = makename();
    this.team1Score = 0;
    this.team2Score = 0;
    this.team1Bags = 0;
    this.team2Bags = 0;
    this.bidding = false;
    this.bidTurn = 0;
 for (var i = 0; i < 4; i++) {
        this.seats.push(new seat(this, i));
        console.log("original seats length "+this.seats.length);
    }
    this.startGame = function() {
        
        // this.resetSeats();
        this.declarePositions();
        this.distributeCards();
        this.emitCards();
       this.bidding=true;;
        this.giveTurn(this.seats[this.bidTurn]);
      
        //this.nextTurn();

    };
      this.declarePositions = function() {
        for (var i = 0; i < this.seats.length; i++) {
            this.seats[i].send("position", this.seats[i].position);
        }
    };
    this.getSeatsInfo = function() {
        var info = [];
        for (var i = 0; i < this.seats.length; i++) {

            info.push({name: this.seats[i].name, position: this.seats()[i].position, 'bid': this.seats()[i].bid});
        }
        return info;
    };


    this.playCard = function(card, seat) {
        if (this.isLegal(card, seat)) {

            this.cards.push({card: card, seat: seat});//lets push an object this time
            if (this.getTable().cards.length == 4)//this is the fourth card played, lets decide who takes the hand
            {
                var winner = this.decideWinner();
                for (var i = 0; i < this.getTable().cards.length; i++) {
                    winner.seat.getFood().push(this.cards[i].card);
                }
                this.getTable().cards = [];
                this.giveTurn(winner.seat);

            }

            else {
                this.nextTurn();
            }
            this.broadcast("cardPlay", {player: seat.position, card: card});
        }
        ;


    };
    this.nextTurn = function() {
        if (this.turn < 3) {
            this.turn++;
        }
        else {
            this.turn = 0;
        }

        if (this.seats[this.turn].cards.length == 0)//he has no cards to play, the game is over
        {
            this.calculatePoints();
            this.startGame();//start the game again
        }
        else {
            for (var i = 0; i < this.getPlayers().length; i++) {
                if (this.turn == i) {
                    this.seats[i].turn = true;
                    this.seats[i].send("turn", {turn: true, bidding: this.bidding});
                }
                else {
                    this.seats[i].turn = false;
                    this.seats[i].send("turn", {turn: false, bidding: false});
                }
            }
        }
        ;
    }

    this.isLegal = function(card, seat) {

        if (seat.turn && this.getTable().cards.length == 0) {
            return true;
        }//first player can play whatever

        else {
            var originalColor = getColor(this.getTable().cards[0].card);//ideally it should match the color of the first card played
            var color = getColor(card);
            if (color == originalColor) {
                return true;
            }
            else {
                for (var i = 0; i < seat.cards.length; i++)//scan the player's cards looking for a card that matches original color
                {
                    if (getColor(seat.cards[i]) == originalColor)   //he has a color of the original color so he must play that
                    {
                        return false;
                    }
                }
                return true;//if not then play whatever... I don't care
            }
        }
        return false;//he must have returned something at this point but if not, return false
    };

    function getValue(num) {
        while ((num - 13) > -1) {
            num = num - 13;
        }
        if ((num + 1) % 13 == 0) {
            this.value = 2;
            return this.value;
        }
        if ((num + 1) % 12 == 0) {
            this.value = 3;
            return this.value;
        }
        if ((num + 1) % 11 == 0) {
            this.value = 4;
            return this.value;
        }
        if ((num + 1) % 10 == 0) {
            this.value = 5;
            return this.value;
        }
        if ((num + 1) % 9 == 0) {
            this.value = 6;
            return this.value;
        }
        if ((num + 1) % 8 == 0) {
            this.value = 7;
            return this.value;
        }
        if ((num + 1) % 7 == 0) {
            this.value = 8;
            return this.value;
        }
        if ((num + 1) % 6 == 0) {
            this.value = 9;
            return this.value;
        }
        if ((num + 1) % 5 == 0) {
            this.value = 10;
            return this.value;
        }
        if ((num + 1) % 4 == 0) {
            this.value = 11;
            return this.value;
        }
        if ((num + 1) % 3 == 0) {
            this.value = 12;
            return this.value;
        }
        if ((num + 1) % 2 == 0) {
            this.value = 13;
            return this.value;
        }
        if (num == 0) {

            this.value = 14;

            return this.value;

        }
    }
    ;

    function getColor(num) {
        var i = 0;
        var color = "";
        while (num - 12 > 0 && i < 4) {
            num = num - 13;
            i++;
        }

        if (i == 0) {
            color = "clubs";
        }
        if (i == 1) {
            color = "spades";
        }
        if (i == 2) {
            color = "diamonds";
        }
        if (i == 3) {
            color = "hearts";
        }
        this.color = color;
        return color;
    }
    ;

    this.giveTurn = function(seat) {

        console.log("next seat cards length is " + seat.getCards().length);
        if (seat.getCards().length == 0 && !bidding)//he has no cards to play, the game is over
        {
            this.calculatePoints();
            this.startGame();//start the game again
        }
        for (var i = 0; i < this.seats.length; i++) {
            if (this.seats[i] == seat) {
                this.seats[i].turn = true;
                this.turn = i;
                this.seats[i].send("turn", {turn: true, bidding: this.bidding});
                //  this.turn = seats[i].position; redundancy
            }
            else {
                this.seats[i].turn = false;
                this.seats[i].send("turn", {turn: false, bidding: this.bidding});
            }
        }

    };

    this.decideWinner = function() {
        var cards = this.cards;
        var originalColor = getColor(cards[0].card);

        if (originalColor != "spades") {//if it is equal to spades anyway avoid an unecessary loop. this if statement can be removed but is there for optimization
            for (var i = 0; i < cards.length; i++)
            {
                if (getColor(cards[i]) == "spades") {//there is a spade here
                    originalColor = "spades";
                }
            }
        }
        //if there is not a spade original color remains the same, if there was at least one spade played the original color turns to spades
        var max = 0;

        for (var i = 0; i < cards.length; i++)
        {//console.log(getValue(cards[i][0]));

            if (getColor(cards[i].card) === originalColor && max == 0) {
                max = cards[i];//if max is not set, set this value to be the max. put the entire object in there
                continue;
            }
            //    console.log(getValue(max[0][0]));
            if (getValue(cards[i].card) > getValue(max.card) && getColor(cards[i].card) == originalColor) {//if you find a bigger card replace the max
                max = cards[i];

            }

        }
        return max;
    }
    this.broadcast = function(key, value) {
        for (var i = 0; i < this.seats.length; i++) {
            this.seats[i].send(key, value);
        }
    }

    function makename() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 7; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }
    ;

    this.calculatePoints = function() {//calculate the teams score
        var team1bid = this.seats[0].bid + this.seats[2].bid;
        var team2bid = this.seats[1].bid + this.seats[3].bid;
        var team1food = (this.seats[0].food.length + this.seats[2].food.length) / 4;
        var team2food = (this.seats[1].food.length + this.seats[3].food.length) / 4;
        if (team1food >= team1bid)
        {
            this.team1Score += team1bid * 10;
            this.team1Bags += team1food - team1bid;
            this.team1Score += team1food - team1bid;// a bag is worth 1 point
            if (this.team1Bags >= 10) {
                this.team1Score -= 100;
                this.team1Bags -= 10;
            }
        } else {//they did not score their bid
            this.team1Score -= team1bid * 10;
        }
        if (team2food >= team2bid)
        {
            this.team2Score += team2bid * 10;
            this.team2Bags += team2food - team2bid;
            this.team2Score += team2food - team2bid;
            if (this.team2Bags >= 10) {
                this.team2Score -= 100;
                this.team2Bags -= 10;
            }
        } else {//they did not score their bid
            this.team2Score -= team2bid * 10;
        }
        for (var i = 0; i < this.seats.length; i++)
        {
//        for(var c=0;c<this.seats[i].food.length;c++)
//        if(getColor(this.seats[i].food[c])=='spades')
            this.seats[i].points = this.seats[i].food.length / 4;
            if (this.seats[i].bid == 0 && this.seats[i].points != 0)//he bid nill but took food
            {
                if (i == 0 || i == 2) {
                    this.team1Score -= 100;
                }
                if (i == 1 || i == 3) {
                    this.team2Score -= 100;
                }
            }
            //end of bid nill but took food

        }//end of loop
        if (this.team1Score <= -200) {
            this.team1Score = -200;
        }
        if (this.team2Score <= -200) {
            this.team2Score = -200;
        }

        console.log("team 1 score " + this.team1Score);
        console.log("team 1 bags " + this.team1Bags);
        console.log("team 2 score " + this.team2Score);
        console.log("team 2 bags " + this.team2Bags);


    };



    this.resetSeats = function() {
        for (var i = 0; i < this.seats.length; i++) {
            if (this.seats[i] != null)
                this.seats[i].resetSeat();
        }
    }
    this.getSeatsInfo = function() {
        var info = [];
        for (var i = 0; i < this.seats.length; i++) {
            if (this.seats[i] != null)
                info.push({name: this.seats[i].name, position: this.seats[i].position, 'bid': this.seats[i].bid});
        }
        return info;
    };

    this.countBid = function() {//check if the bidding phase is over or not
        var done = true;
        for (var i = 0; i < this.seats.length; i++) {
            if (this.seats[i].bid == null || this.seats[i].bid == '') {
                done = false;
            }
        }
        if (done) {
            this.bidding = false;
            this.bidTurn++;
            if (this.bidTurn == 4)
            {
                this.bidTurn = 0;
            }
        }
        console.log("done " + done);
        this.nextTurn();
    }

    this.addPlayer = function(player) {

        for (var i = 0; i < this.seats.length; i++) {
            if (this.seats[i].type == 0) {
                this.seats[i].type = 1;
                this.seats[i].player = player;
                 this.seats[i].name = player.name;
                 player.seat= this.seats[i];
                this.seats[i].send('joinTable', {tableName: this.tableName, position: this.seats[i].position, players: this.getSeatsInfo()});
                this.broadcast('playerJoined', {tableName: this.tableName, players: this.getSeatsInfo()});
                if(!this.hasVacant()){this.startGame();}
                break;
            }
            if (i == 4) {
                return false;
            }
        }
    };
    this.hasVacant=function(){
           console.log("vacancy  seats length "+this.seats.length);
         for(var i=0;i<this.seats.length;i++){
             console.log(this.seats[i].type);
     if(this.seats[i].type==0){return true;}
    
    } 
         return false;
    }
     
    this.turns=function(){
             for (var i = 0; i < this.seats.length; i++) {
            if (i==this.turn) {
                this.seats[i].send("turn", {turn: true, bidding: this.bidding});
            }
            else {
                this.seats[i].send("turn", {turn: false, bidding: this.bidding});
            }
        }
    }
    //********
       this.emitCards = function() {


        for (var i = 0; i < this.seats.length; i++) {
            if (this.seats[i] != null)
                this.seats[i].send('cards', this.seats[i].cards);
        }

    }

    this.distributeCards = function() {
        this.seats[0].cards = [];
        this.seats[1].cards = [];
        this.seats[2].cards = [];
        this.seats[3].cards = [];
        var cards = [];
        for (var i = 0; i < 52; i++) {
            cards[i] = i;

        }

        shuffle(cards);
        /* cards = cards.sort(function(a, b) {
         return a - b
         });
         */
        for (var i = 0; i < this.seats.length; i++) {
            if (i == 0) {
                this.seats[i].cards = fromTo(cards, 0, 12)
            }
            if (i == 1) {
                this.seats[i].cards = fromTo(cards, 13, 25)
            }
            if (i == 2) {
                this.seats[i].cards = fromTo(cards, 26, 38)
            }
            if (i == 3) {
                this.seats[i].cards = fromTo(cards, 39, 51)
            }



        }
    }
    function fromTo(array, from, to) {
        var result = [];
        for (var i = from; i <= to; i++) {
            result.push(array[i]);
        }
        return result;
    }
    function shuffle(o) { //v1.0
        for (var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x)
            ;
        return o;
    };
    //***********
    
       this.broadcastTableStatus = function() {
      this.emitCards();
this.broadcast('playerJoined', {tableName: this.tableName, players: this.getSeatsInfo()});
this.turns();
    for(var i=0;i<this.seats.length;i++){
       // console.log(this.tableName+" "+this.players[i].position+"bidding "+this.players[i].bid);
         
    } };
    
    var me = this;
    setInterval(function() {
        me.broadcastTableStatus();
    }, 5000);
}
module.exports = table;
