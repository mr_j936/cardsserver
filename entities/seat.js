var hollow = require('./hollowPlayer');


var seat = function(table, position, redis) {//this class is the link between human/AI users and the table. The table does not care if this user is an AI or human 

    this.position = position;
    this.bid = null;
    this.id = makename();//we should broadcast seat names instead of table name so that we can follow the pattern of player>seat>table;
    this.food = [];
    this.turn = false;
    this.points = 0;
    this.table = table;
    this.cards = [];
    this.name = "";
    this.type = 0;//0 for hollow, 1 for human and 2 for ai
    this.player = new hollow();//could have an AI user here. It starts with a hollow player class that prevents shit from crashing
    this.pubClient = redis.createClient();
    this.subClient = redis.createClient();
    this.playerId = null;
    this.subClient.subscribe(this.id);
    var me = this;
    this.unsubscribe = function() {
        this.subClient.unsubscribe('message');
        this.id = null;
        this.points = null;
        this.table = null;
        this.name = null;
        this.pubClient = null;
        this.subClient = null;
        this.playerId = null;

    };
    this.subscription = this.subClient.on('message', function(channel, message) {

        var data = JSON.parse(message);
        switch (data.key) {
            case 'playCard':
                me.playCard(data.value);
                break;

            case 'addPlayer':
                me.addPlayer(data.value);
                break;
            case 'bid':
                me.bid = data.value;
                me.table.countBid();
                break;
            case 'vacateSeat':
                // console.log("vacating seat outside if statement");
                // console.log(data.value.playerId);
                if (data.value.playerId == me.playerId) {
                    //   console.log("vacating seat");
                    me.vacateSeat();
                }
                break;

        }
    });

    this.addPlayer = function(message) {

        if (this.type == 0)
        {
            this.type = 1;
            this.playerId = message.playerId;
            this.name = message.name;
            this.send('seat', {seatId: this.id});
            //   console.log("creator id is " + this.name);
            this.send('joinTable', {tableName: this.table.tableName, position: this.position, players: this.table.getSeatsInfo()});

            //     player.seat= this;
            if (this.table.hasVacant() == false && this.table.started == false) {
                this.table.startGame();
                //  console.log("starting game from seats");
            }
            var message = JSON.stringify({key: 'updateGame', value: {tableName: me.table.tableName, seatsInfo: me.table.getSeatsInfo(), visibility: me.table.visibility, maxScore: me.table.maxScore, vacant: me.table.hasVacant()}});
            me.pubClient.publish('main', message)//if we create a game or a player is added this method is called, to update games everywhere and trigger a broadcast
        }
//send that the table is full, or try to manage a spot for the player on another seat, which could be doen directly since all seats are on the same server, and then return table is full
    }

//        this.addPlayer=function(player){
//        this.type = 1;
//               this.player = player;
//               this.name = player.name;
//                 player.seat= this;
//                 if(this.table.hasVacant()==false){
//                     this.table.startGame();
//                     console.log("starting game");
//                 }
//             }

    this.getCards = function() {
        return this.cards;
    }
    this.hasCard = function(card) {
        for (var i = 0; i < this.cards.length; i++) {
            if (this.cards[i] == card) {
                return true;
            }
        }
        return false;
    }
    this.removeCard = function(card) {
        for (var i = 0; i < this.cards.length; i++) {
            if (this.cards[i] == card) {
                this.cards.splice(i, 1);

            }
        }
        return false;
    }
    this.vacateSeat = function() {//maybe tell other players and or the table that the seat was vacated; Or mayeb this order should come from the table directly, and so notifying players is not our concern
        this.name = '';
        this.player = new hollow();
        this.type = 0;//make it type 0 again
        this.playerId = null;
        if (table.empty()) {
            console.log("yupe, kill it");
            var message = JSON.stringify({key: 'remove', value: {tableName: me.table.tableName}});
            me.pubClient.publish('main', message)//if we create a game or a player is added this method is called, to update games everywhere and trigger a broadcast

        }
        else {
            var message = JSON.stringify({key: 'updateGame', value: {tableName: me.table.tableName, seatsInfo: me.table.getSeatsInfo(), visibility: me.table.visibility, maxScore: me.table.maxScore, vacant: me.table.hasVacant()}});
            me.pubClient.publish('main', message)//if we create a game or a player is added this method is called, to update games everywhere and trigger a broadcast
        }
        //unsubscribe redis too don't forget
    }

    //needs to be moved to redis too


    this.setTurn = function(turn, bidding) {//tell the dumb user he can play now, or not
        this.turn = turn;
        //  this.player.setTurn(turn, bidding);
        this.send('turn', {turn: turn, bidding: bidding});
    }

    this.playCard = function(card) {//tell the table to play the card. The player wil linvoke this call
        this.table.playCard(card, this);
    }

    this.getSeatsInfo = function() {
        return    this.table.getSeatsInfo();
    }

    this.send = function(key, value)//abstract all messages and commands that were supposed to be sent to the players via redis through this method
    {   //this.player.send(key,value);
        if (this.type == 1) {
            this.pubClient.publish(this.playerId, JSON.stringify({key: key, value: value}));

        }
    }
    this.resetSeat = function() {
        this.bid = null;
        this.food = [];
        this.turn = false;
        this.points = 0;
        this.cards = [];
    }



    function makename() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 9; i++)//set to 9 instead of 7
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }
    ;
}

module.exports = seat;