var seat = function() {//this class is the link between human/AI users and the table. The table does not care if this user is an AI or human 

    this.position = null;
    this.bid = null;
    this.score;
    this.food = [];
    this.turn = false;
    this.points = 0;
    this.table = null;
    this.cards = [];
    this.name = "";
    this.type=0;//0 for hollow, 1 for human and 2 for ai
    this.user=null;//could have an AI user here
    
    this.vacateSeat=function(){
                this.name='';
        this.player=null;
    }
    this.setPlayer=function(player){//put a player here, could be an AI or a human, inherit its name
        this.name=player.name;
        this.player=player;
    }
    
    this.setTurn=function(turn){//tell the dumb user he can play now, or not
        this.turn=turn;
        this.user.setTurn(turn);
    }
    
    this.playCard=function(card){//tell the table to play the card. The player wil linvoke this call
        this.table.playCard(card);
    }
}

module.exports = seat;