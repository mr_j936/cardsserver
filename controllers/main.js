var io = require('socket.io')(81);
var redis = require('redis');
var request = require("request");
var humanPlayer = require('./../entities/humanPlayer');
var table = require('./../entities/table');
var util = require('util');
var game = require('./../entities/game');
var game1 = new game();
this.pubClient = redis.createClient();
this.subClient = redis.createClient();
tables = [];
//this.id = makename();
this.subClient.subscribe('main');
var me = this;
setTimeout(function() {
    console.log(util.inspect(process.memoryUsage()));
}, 1000);
me.subClient.on('message', function(channel, message) {
    var data = JSON.parse(message);
    switch (data.key) {
        case 'roomChat':
            io.sockets.emit('roomChat', data.value);//since it is now outside socket.onconnection the redis events will be received once per main method and broadcasted to all.
            break; // Also, we will now have a handful of redis channels instead of one per player
        case 'tablelist':
            io.sockets.emit('tables', game1.getTables());
            break;
        case 'updateGame':
            console.log("yo, new game here");
            game1.updateGame(data.key, data.value);
            io.sockets.emit('tables', game1.getTables());
            break;
        case 'remove':
            removeTable(data.value.tableName);
            game1.updateGame(data.key, data.value);
            io.sockets.emit('tables', game1.getTables());
            break;
    }



});
function removeTable(tablename) {
    for (var i = 0; i < this.tables.length; i++) {
        if (tables[i].tableName == tablename)
        {
            console.log("cleaning up");
            tables[i].cleanUp();
            tables.splice(i, 1);
            //  global.gc();
            setTimeout(function() {
                console.log(util.inspect(process.memoryUsage()));
            }, 2000);
            return;
        }
    }
}

io.on('connection', function(socket) {

    console.log("player connected here");
    var player1 = new humanPlayer(socket, makename(), redis);
    player1.socket = socket;
    socket.player = player1;
    // socket.player = player1;

    console.log("connected");
//    if (table1.players.length < 4) {
//        table1.addPlayer(player1);
//        if (table1.players.length == 4) {
//            table1.startGame();
//        } 
//    }
    socket.on("accessToken", function(data)
    {//we need some code to actually check the user's identity
        //  console.log(data.accessToken);
        console.log(data.accessToken);
        if (typeof data.accessToken != 'undefined' && data.accessToken != null) {
            if (data.accessToken == "G")
            {
                socket.emit('authorised', true);
            }
            else {
                request("https://graph.facebook.com/me?access_token=" + data.accessToken, function(error, response, body) {
                    var info = JSON.parse(body);
                    if (typeof info.first_name != 'undefined' && info.first_name != null && typeof socket.player != 'undefined') {
                        socket.player.name = info.first_name;

                        socket.emit('authorised', true);
                    } else {
                        socket.emit('disconnect')
                    }
                });
            }
        }
    });
    socket.on('nickname', function(data) {
        player1.name = data;//we might restrict this to guests only
        socket.emit('nickname', data);

    });

    socket.on('createTable', function(data) {

        if (socket.player.seat == null && data.maxScore !== 'undefined' && data.maxScore != null && data.visibility !== 'undefined' && data.visibility != null)
        {
            table1 = new table(redis);
            table1.game = game1;
            table1.visibility = data.visibility;
            table1.maxScore = data.maxScore;
            table1.addPlayer(socket.player);//we know they are on the same server, he just created the game, so we can cheat and do this
            //console.log("added player and seat is " + socket.player.seat);
            table1.owner = socket.player;
            tables.push(table1);
            table1 = null;
            setTimeout(function() {
                console.log(util.inspect(process.memoryUsage()));
            }, 2000);
//table 1 is freferenced inside its seat, and the seat is referenced inside the owner, it should not cause a leak 
            // game1.addTable(table1);         
        }
    });



    socket.on('roomChat', function(data) {//emited when chating in room
        console.log(data);
        //   io.sockets.emit('roomChat', socket.player.name + ": " + data);
        var message = JSON.stringify({key: 'roomChat', value: socket.player.name + ": " + data});
        me.pubClient.publish('main', message)//if we create a game or a player is added this method is called, to update games everywhere and trigger a broadcast

    });
    socket.on('tablelist', function(data) {
        if (socket.player.seatId != null) {
            socket.player.leave();
            io.sockets.emit('tables', game1.getTables());
        }

        if (data) {
            socket.emit('tables', game1.getTables());
        }
        // console.log(game1.getTables());
    });

    socket.on('joinSeat', function(data) {//emited when joining table
        // var table = game1.findTable(data);

        var message = JSON.stringify({key: 'addPlayer', value: {name: socket.player.name, playerId: socket.player.id}});
        me.pubClient.publish(data, message);//data is the seat's name we will bind to
        //  seat.addPlayer(socket.player);
        //   console.log("added player and seat is " + socket.player.seat);
        //   seat.send('joinTable', {tableName: seat.table.tableName, position: seat.position, players: seat.table.getSeatsInfo()});
        //   this.broadcast('playerJoined', {tableName: this.tableName, players: this.getSeatsInfo()});

        io.sockets.emit('tables', game1.getTables());



    });
    socket.on('cards', function(data) {
        if (socket.player.cards.length > 0) {
            socket.emit("cards", socket.player.cards);
        }
    });
    socket.on('disconnect', function() {
        console.log(socket.player.name + "disconnected");

        if (socket.player.seatId != null) {
            socket.player.leave();
            io.sockets.emit('tables', game1.getTables());
        }

        //  delete socket.player;

        console.log('disconnected event');
        socket.player = null;
        socket = null;
        player1.unsubscribe();
        player1.pubClient = null;
        player1.subClient = null;
        player1 = null;

    });


//    socket.on('playCard', function(data) {
//    
//       console.log("attempting to play card "+data)
//          socket.player.seat.playCard(data);
//      
//    });

//      socket.on('bid', function(data) {
//        //check if he is allowed to play the card
//        console.log("seat is "+socket.player.seat);
//        if(socket.player.seat!=null&&socket.player.seat.turn&&socket.player.seat.table.bidding){
//        console.log("ok");
//              socket.player.seat.bid=data;
//socket.emit('playerJoined', {tableName: socket.player.seat.table.tableName, players:socket.player.seat.table.getSeatsInfo()});
//socket.player.seat.table.countBid();
//        }});

});
function makename() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 4; i++)//increased from 7 to 10 to make it even less likely that 2 players will have same id, and so it would not cross with table and seat names
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}
;

