var hollow = require('./hollowPlayer');
var seat = function(table,position) {//this class is the link between human/AI users and the table. The table does not care if this user is an AI or human 

    this.position = position;
    this.bid = null;
    this.food = [];
    this.turn = false;
    this.points = 0;
    this.table = table;
    this.cards = [];
    this.name = "";
    this.type=0;//0 for hollow, 1 for human and 2 for ai
    this.player=new hollow();//could have an AI user here. It starts with a hollow player class that prevents shit from crashing
    
    this.getCards=function(){return this.cards;}
    
    this.vacateSeat=function(){//maybe tell other players and or the table that the seat was vacated; Or mayeb this order should come from the table directly, and so notifying players is not our concern
                this.name='';
        this.player=new hollow();
    }
    this.setPlayer=function(player){//put a player here, could be an AI or a human, inherit its name
        this.name=player.name;
        this.player=player;
        this.player.joinTable(this.table.tableName);
    }
    
    this.setTurn=function(turn,bidding){//tell the dumb user he can play now, or not
        this.turn=turn;
        this.user.setTurn(turn,bidding);
    }
    
    this.playCard=function(card){//tell the table to play the card. The player wil linvoke this call
        this.table.playCard(card,this);
    }
    
    this.getSeatsInfo=function(){
    return    this.table.getSeatsInfo();
    }
    
    this.send=function(key,value)
    {this.player.send(key,value);
        
    }
    this.resetSeat=function(){
        this.bid = null;
    this.food = [];
    this.turn = false;
    this.points=0;
    this.cards=[];
    }
}

module.exports = seat;