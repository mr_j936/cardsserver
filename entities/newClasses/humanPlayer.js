var humanPlayer = function(socket,name) {//this class is the link between human/AI users and the table. The table does not care if this user is an AI or human 
this.name=name;
this.seat=null; //the seat, if he is on a table he will have a seat to control the table. Player has one seat only. 
this.socket=socket;

 

playCard=function(card){
    this.seat.playCard(card);
}
setTurn=function(turn,bidding){
  this.send("turn", {turn: turn, bidding: bidding});  
}

this.joinTable=function(){
   this.socket.emit('joinTable', {tableName: this.seat.table.tableName, position: this.seat.position, players: this.seat.getSeatsInfo()}); 
}

this.send=function(key,value){
    this.socket.emit(key,value);
}




}

module.exports = humanPlayer;